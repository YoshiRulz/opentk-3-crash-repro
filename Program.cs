using System;
using System.Diagnostics;
using System.Threading;

namespace BizHawk.Experiment.OTK3Repro
{
	public static class Program
	{
		private static readonly string ESC = DistinctKey.Escape.ToString();

		public static void Main()
		{
			static void ProgramRunLoop()
			{
				while (true)
				{
					InputEvent ie;
					while ((ie = Input.Instance.DequeueEvent()) != null)
					{
						Console.WriteLine(ie);
						if (ie.LogicalButton.Button == ESC) return; // break 2
					}
					Thread.Sleep(10);
				}
			}
			ProgramRunLoop();
			Process.GetCurrentProcess().Kill();
		}
	}
}
