## Purpose

A BizHawk user by the name of juef reported [this crash](https://pastebin.com/yv7JaZmT) which I believe is related to [opentk/opentk#937](https://github.com/opentk/opentk/issues/937).

## The program

It prints which keys are pressed or released, 'nuff said.
