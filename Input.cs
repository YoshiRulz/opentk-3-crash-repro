﻿#nullable disable

using System.Collections.Generic;
using System.Threading;

namespace BizHawk.Experiment.OTK3Repro
{
	public class Input
	{
		public static readonly Input Instance = new();

		private readonly Thread _updateThread;

		private Input()
		{
			OpenTKConfigurator.EnsureConfigurated();
			OTK_Keyboard.Initialize();
			_updateThread = new Thread(UpdateThreadProc)
			{
				IsBackground = true,
				Priority = ThreadPriority.AboveNormal // why not? this thread shouldn't be very heavy duty, and we want it to be responsive
			};
			_updateThread.Start();
		}

		private readonly Dictionary<string, LogicalButton> _modifierState = new();

		private readonly Dictionary<string, bool> _lastState = new();

		private bool GetState(string s) => _lastState.TryGetValue(s, out var b) && b;

		private bool _ignoreEventsNextPoll;

		private void HandleButton(string button, bool newState, ClientInputFocus source)
		{
			var currentModifier = button switch
			{
//				"LeftWin" => ModifierKey.Win,
//				"RightWin" => ModifierKey.Win,
				"LeftShift" => ModifierKey.Shift,
				"RightShift" => ModifierKey.Shift,
				"LeftCtrl" => ModifierKey.Control,
				"RightCtrl" => ModifierKey.Control,
				"LeftAlt" => ModifierKey.Alt,
				"RightAlt" => ModifierKey.Alt,
				_ => ModifierKey.None
			};
			if (GetState(button) == newState) return;

			// apply
			// NOTE: this is not quite right. if someone held leftshift+rightshift it would be broken. seems unlikely, though.
			if (currentModifier != ModifierKey.None)
			{
				if (newState)
					_modifiers |= currentModifier;
				else
					_modifiers &= ~currentModifier;
			}

			// don't generate events for things like Ctrl+LeftControl
			ModifierKey mods = _modifiers;
			if (currentModifier != ModifierKey.None)
				mods &= ~currentModifier;

			var ie = new InputEvent
				{
					EventType = newState ? InputEventType.Press : InputEventType.Release,
					LogicalButton = new LogicalButton(button, mods),
					Source = source
				};
			_lastState[button] = newState;

			// track the pressed events with modifiers that we send so that we can send corresponding unpresses with modifiers
			// this is an interesting idea, which we may need later, but not yet.
			// for example, you may see this series of events: press:ctrl+c, release:ctrl, release:c
			// but you might would rather have press:ctrl+c, release:ctrl+c
			// this code relates the releases to the original presses.
			// UPDATE - this is necessary for the frame advance key, which has a special meaning when it gets stuck down
			// so, i am adding it as of 11-sep-2011
			if (newState)
			{
				_modifierState[button] = ie.LogicalButton;
			}
			else
			{
				if (_modifierState.TryGetValue(button, out var buttonModifierState))
				{
					if (buttonModifierState != ie.LogicalButton && !_ignoreEventsNextPoll)
					{
						_newEvents.Add(
							new InputEvent
							{
								LogicalButton = buttonModifierState,
								EventType = InputEventType.Release,
								Source = source
							});
					}
					_modifierState.Remove(button);
				}
			}

			if (!_ignoreEventsNextPoll)
			{
				_newEvents.Add(ie);
			}
		}

		private ModifierKey _modifiers;
		private readonly List<InputEvent> _newEvents = new();

		private readonly Queue<InputEvent> _inputEvents = new();
		public InputEvent DequeueEvent()
		{
			lock (this)
			{
				return _inputEvents.Count == 0 ? null : _inputEvents.Dequeue();
			}
		}

		private void EnqueueEvent(InputEvent ie)
		{
			lock (this)
			{
				_inputEvents.Enqueue(ie);
			}
		}

		private void UpdateThreadProc()
		{
			static string KeyName(DistinctKey k) => k switch
			{
				DistinctKey.Back => "Backspace",
				DistinctKey.Enter => "Enter",
				DistinctKey.CapsLock => "CapsLock",
				DistinctKey.PageDown => "PageDown",
				DistinctKey.D0 => "Number0",
				DistinctKey.D1 => "Number1",
				DistinctKey.D2 => "Number2",
				DistinctKey.D3 => "Number3",
				DistinctKey.D4 => "Number4",
				DistinctKey.D5 => "Number5",
				DistinctKey.D6 => "Number6",
				DistinctKey.D7 => "Number7",
				DistinctKey.D8 => "Number8",
				DistinctKey.D9 => "Number9",
				DistinctKey.LWin => "LeftWin",
				DistinctKey.RWin => "RightWin",
				DistinctKey.NumPad0 => "Keypad0",
				DistinctKey.NumPad1 => "Keypad1",
				DistinctKey.NumPad2 => "Keypad2",
				DistinctKey.NumPad3 => "Keypad3",
				DistinctKey.NumPad4 => "Keypad4",
				DistinctKey.NumPad5 => "Keypad5",
				DistinctKey.NumPad6 => "Keypad6",
				DistinctKey.NumPad7 => "Keypad7",
				DistinctKey.NumPad8 => "Keypad8",
				DistinctKey.NumPad9 => "Keypad9",
				DistinctKey.Multiply => "KeypadMultiply",
				DistinctKey.Add => "KeypadAdd",
				DistinctKey.Separator => "KeypadComma",
				DistinctKey.Subtract => "KeypadSubtract",
				DistinctKey.Decimal => "KeypadDecimal",
				DistinctKey.Divide => "KeypadDivide",
				DistinctKey.Scroll => "ScrollLock",
				DistinctKey.OemSemicolon => "Semicolon",
				DistinctKey.OemPlus => "Equals",
				DistinctKey.OemComma => "Comma",
				DistinctKey.OemMinus => "Minus",
				DistinctKey.OemPeriod => "Period",
				DistinctKey.OemQuestion => "Slash",
				DistinctKey.OemTilde => "Backtick",
				DistinctKey.OemOpenBrackets => "LeftBracket",
				DistinctKey.OemPipe => "Backslash",
				DistinctKey.OemCloseBrackets => "RightBracket",
				DistinctKey.OemQuotes => "Apostrophe",
				DistinctKey.OemBackslash => "OEM102",
				DistinctKey.NumPadEnter => "KeypadEnter",
				_ => k.ToString()
			};
			while (true)
			{
				var keyEvents = OTK_Keyboard.Update();

				//this block is going to massively modify data structures that the binding method uses, so we have to lock it all
				lock (this)
				{
					_newEvents.Clear();

					//analyze keys
					foreach (var ke in keyEvents) HandleButton(KeyName(ke.Key), ke.Pressed, ClientInputFocus.Keyboard);

					if (_newEvents.Count != 0)
					{
						//WHAT!? WE SHOULD NOT BE SO NAIVELY TOUCHING MAINFORM FROM THE INPUTTHREAD. ITS BUSY RUNNING.
						AllowInput allowInput = AllowInput.All;

						foreach (var ie in _newEvents)
						{
							//events are swallowed in some cases:
							if (ie.LogicalButton.Alt && ShouldSwallow(AllowInput.All, ie))
								continue;
							if (ie.EventType == InputEventType.Press && ShouldSwallow(allowInput, ie))
								continue;

							EnqueueEvent(ie);
						}
					}

					_ignoreEventsNextPoll = false;
				} //lock(this)

				//arbitrary selection of polling frequency:
				Thread.Sleep(10);
			}
		}

		private static bool ShouldSwallow(AllowInput allowInput, InputEvent inputEvent)
		{
			return allowInput == AllowInput.None || (allowInput == AllowInput.OnlyController && inputEvent.Source != ClientInputFocus.Pad);
		}
	}
}
